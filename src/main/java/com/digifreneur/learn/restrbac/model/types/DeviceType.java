package com.digifreneur.learn.restrbac.model.types;

public enum DeviceType {
    /**
     * Android Device Type
     */
    DEVICE_TYPE_ANDROID,

    /**
     * IOS Device Type
     */
    DEVICE_TYPE_IOS,
}
