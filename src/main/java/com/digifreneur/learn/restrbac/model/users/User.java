package com.digifreneur.learn.restrbac.model.users;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.digifreneur.learn.restrbac.model.audit.UserDateAudit;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.annotations.NaturalId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@Table(
    name = "users", 
    uniqueConstraints = { 
        @UniqueConstraint(columnNames = { "username" }),
        @UniqueConstraint(columnNames = { "email" }) }
    )
public class User extends UserDateAudit{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @NotBlank
    @Column(name = "first_name")
    @Size(max = 50)
    private String firstName;

    @NotBlank
    @Column(name = "last_name")
    @Size(max = 50)
    private String lastName;

    @NotBlank
    @Column(name = "username", unique = true)
    @Size(max = 20)
    private String username;

    @NotBlank
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Size(max = 128)
    @Column(name = "password")
    private String password;

    @NotBlank
    @NaturalId
    @Size(max = 65)
    @Column(name = "email", unique = true)
    @Email
    private String email;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(name = "phone", nullable = true)
    private String phone;

    @Column(name = "website", nullable = true)
    private String website;

    @Column(name = "is_active", nullable = false)
    private Boolean active;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "user_role", 
        joinColumns = { 
            @JoinColumn(name = "user_id") 
        }, 
        inverseJoinColumns = {
            @JoinColumn(name = "role_id") 
        }
    )
    private List<Role> roles;

    @Column(name = "is_email_verified", nullable = false)
    private Boolean isEmailVerified;

    public User(String firstName, String lastName, String username, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles == null ? null : new ArrayList<>(roles);
    }

    public void setRoles(List<Role> roles) {
        if (roles == null) {
            this.roles = null;
        } else {
            this.roles = Collections.unmodifiableList(roles);
        }
    }

    public String getFullName() {
        return firstName != null ? firstName.concat(" ").concat(lastName) : "";
    }
}
