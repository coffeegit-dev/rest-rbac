package com.digifreneur.learn.restrbac.dto.mapper;

import com.digifreneur.learn.restrbac.dto.model.user.RoleDto;
import com.digifreneur.learn.restrbac.dto.model.user.UserDto;
import com.digifreneur.learn.restrbac.model.users.User;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class UserMapper {
    
    @Autowired
    private static ModelMapper modelMapper;

    public static UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setPhone(user.getPhone());
        userDto.setWebsite(user.getWebsite());
        userDto.setRoles(new ArrayList<RoleDto>(user
                .getRoles()
                .stream()
                .map(role -> new ModelMapper().map(role, RoleDto.class))
                .collect(Collectors.toList())));
        return userDto;
    }

    // not used, not a dynamic solutions
    public static User convertToEntity(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);

        return user;
    }
}
