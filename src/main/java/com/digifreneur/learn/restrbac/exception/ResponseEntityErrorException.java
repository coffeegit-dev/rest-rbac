package com.digifreneur.learn.restrbac.exception;

import com.digifreneur.learn.restrbac.payload.ApiResponse;

import org.springframework.http.ResponseEntity;

public class ResponseEntityErrorException extends RuntimeException{
    private static final long serialVersionUID = -4485708018686239594L;
    
    private transient ResponseEntity<ApiResponse> apiResponse;

    public ResponseEntityErrorException(ResponseEntity<ApiResponse> apiResponse) {
        this.apiResponse = apiResponse;
    }

    public ResponseEntity<ApiResponse> getApiResponse() {
        return apiResponse;
    }
}
