package com.digifreneur.learn.restrbac.exception;

import org.springframework.http.HttpStatus;

public class DigiException extends RuntimeException {
    private static final long serialVersionUID = -6593330219878485669L;
    
    private final HttpStatus status;
    private final String message;

    public DigiException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public DigiException(HttpStatus status, String message, Throwable cause) {
        super(cause);
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
