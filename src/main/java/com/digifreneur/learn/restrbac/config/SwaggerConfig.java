package com.digifreneur.learn.restrbac.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    /**
     * Group RBAC contains operations related to reservations and agency management
     */
    @Bean
    public Docket swaggerRBACApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("RBAC").select()
                .apis(RequestHandlerSelectors.basePackage("com.digifreneur.learn.restrbac.controller.v1"))
                .paths(PathSelectors.any()).build().apiInfo(apiInfo()).securitySchemes(Arrays.asList(apiKey()));
    }

    /**
     * Group User contains operations related to user management such as
     * login/logout
     */
    @Bean
    public Docket swaggerUserApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("User").select()
                .apis(RequestHandlerSelectors.basePackage("com.digifreneur.learn.restrbac.controller.v1.faker"))
                .paths(PathSelectors.any()).build().apiInfo(apiInfo()).securitySchemes(Arrays.asList(apiKey()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("REST RBAC").description("Spring Boot starter kit RBAC for application.")
                .termsOfServiceUrl("")
                .contact(new Contact("Sigit Prayitno", "https://digifreneur.com", "digifreneurid@gmail.com"))
                .license("Apache License Version 2.0").licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .version("0.0.1").build();
    }

    private ApiKey apiKey() {
        return new ApiKey("apiKey", "Authorization", "header");
    }
}
