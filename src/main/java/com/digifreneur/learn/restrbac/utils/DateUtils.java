package com.digifreneur.learn.restrbac.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static final SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static final SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Returns today's date as java.util.Date object
     *
     * @return today's date as java.util.Date object
     */
    public static Date today() {
        return new Date();
    }

    /**
     * Returns today's date as yyyy-MM-dd format
     *
     * @return today's date as yyyy-MM-dd format
     */
    public static String dbTodayStr() {
        return dbDateFormat.format(today());
    }

    /**
     * Returns today's date as dd-MM-yyyy format
     *
     * @return today's date as dd-MM-yyyy format
     */
    public static String humanTodayStr() {
        return humanDateFormat.format(today());
    }

    /**
     * Returns the formatted String date for the passed java.util.Date object
     *
     * @param date
     * @return
     */
    public static String dbFormattedDate(Date date) {
        return date != null ? dbDateFormat.format(date) : dbTodayStr();
    }

    /**
     * Returns the formatted String date for the passed java.util.Date object
     *
     * @param date
     * @return
     */
    public static String humanFormattedDate(Date date) {
        return date != null ? humanDateFormat.format(date) : humanTodayStr();
    }
}
