package com.digifreneur.learn.restrbac.utils;

import java.util.UUID;

public class AppUtils {

    private AppUtils() {
        throw new UnsupportedOperationException("Cannot instantiate a Util class");
    }
    
    public static String generateRandomUuid() {
        return UUID.randomUUID().toString();
    }
}
