# Spring Boot Boilerplate

Build Restful API using Spring Boot, Mysql, JPA and Hibernate.

## Steps to Setup

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.

## Contributors
[Sigit Prayitno](https://www.linkedin.com/in/sigitprayitno)

Please make sure to update tests as appropriate.

## License
This project is licensed under the terms of the [MIT](https://choosealicense.com/licenses/mit/) license.